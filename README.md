
# README #

Pre Proposal
https://docs.google.com/document/d/1AywrFe1WJ68whyIz5KgYNJ2vBSq9cg7bBCvn_ifNh9Y/edit

Final Proposal
https://docs.google.com/document/d/1sxoVLECPKFD055G5909FVPu3SHrDbd-HjdNhaTdmRSk/edit

Design
https://docs.google.com/document/d/10fuDTaftGJitN2ix7XM3mq6Q1jz4ZlevIA94la4YQe4/edit

Report
https://docs.google.com/document/d/1WCfJmFRgu1d8T8BnVNXCgBojYWSPSMcC_78V1vHH168/edit?usp=sharing

Poster slides
https://docs.google.com/presentation/d/1x0CLRvZ2ZaH1g_aLAHh4Gdh-h7HmWLwvPr3tPLKerYw/edit?usp=sharing

Video
gitrepository / fuzzydemo.mp4

Demo
download and execute "npm install; npm start" or visit http://sheridanthirsk.com/fuzzybrain .

### What is this repository for? ###

* People who want to learn a very basic AI without hooking into too much game code.
* Using chaining construct:  Query.filter(filterFood).sort(sortingDistance).max(one).map(scoringDistance)

# Fuzzybrainular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

The most relevant scripts for this class are fuzzybrain.ts fuzzylib.ts fuzzyquery.ts

### Who do I talk to? ###

* Sheridan Thirsk