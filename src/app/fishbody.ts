/** The fish body is the minimal amount of information needed by brains to make an informed decision of their neighbours */
export class Fishbody {
    public id:number;
    public hp:number;
    public energy:number;
    public hunger:number;
    public size:number;
    public x:number = 0;
    public y:number = 0;
    public food:boolean;
    public maxenergy:number = 2000;
    public maxhunger:number = 2000;

    constructor(id:number, size:number) {
        this.id = id;
        this.hp = Math.min(400+size*80,1000);
        this.hunger = 500;
        this.energy = 1000;
        this.size = size;
    }

    /** copy data and return true if something changed */
    clone(other:Fishbody): boolean {
        let different = false;
        if(this.id != other.id) {
            this.id = other.id;
            different = true;
        }
        if(this.hp != other.hp) {
            this.hp = other.hp;
            different = true;
        }
        if(this.size != other.size) {
            this.size = other.size;
            different = true;
        }
        if(this.energy != other.energy) {
            this.energy = other.energy;
            different = true;
        }
        if(this.hunger != other.hunger) {
            this.hunger = other.hunger;
            different = true;
        }
        return different;
    }
}
