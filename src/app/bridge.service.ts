/** this class holds all possible input and output data that interacts with angular text fields and widgets */

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { Fishbody } from './fishbody'
import { Fishbrain } from './fishbrain'
import { Fuzzybrain } from './fuzzybrain'


@Injectable()
export class BridgeService {

  public currentTime: number = 0;
  public focusedBody: Fishbody;
  public focusedBrain: Fuzzybrain;
  public focusedSize: number = 0;
  public bestSize: number = 0;
  public bodySubject: Subject<Fishbody>;
  public brainSubject: Subject<Fuzzybrain>;
  public showScrip: boolean;
  public showVartune: boolean;
  public humanMode: boolean;
  public editMode: boolean;
  public scripContents: string;
  public alphaQueryString: string;
  public bravoQueryString: string;
  public charlieQueryString: string;
  public romeoDecideString: string;
  public sierraDecideString: string;
  public tangoDecideString: string;
  public funcString: string;
  public paramString: string;

  public romeoExecuted: number = 0;
  public sierraExecuted: number = 0;
  public tangoExecuted: number = 0;
  public romeoAlphaRelation: number = 1;
  public romeoBravoRelation: number = 0;
  public romeoCharlieRelation: number = 0;
  public romeoEnabled:boolean = true;
  public sierraAlphaRelation: number = 0;
  public sierraBravoRelation: number = 1;
  public sierraCharlieRelation: number = 0;
  public sierraEnabled:boolean = false;
  public tangoAlphaRelation: number = 0;
  public tangoBravoRelation: number = 0;
  public tangoCharlieRelation: number = 1;
  public tangoEnabled:boolean = false;
  constructor() {
    this.showScrip = true;
    this.showVartune = false;
    this.focusedBody = new Fishbody(0, 0);
    this.focusedBrain = null;
    this.bodySubject = new Subject<Fishbody>();
    this.brainSubject = new Subject<Fuzzybrain>();
  }
  tick(millis: number) {
    this.currentTime += millis;
  }
  /** Send initial message to the Subject subscribers */
  firstReport(allbrains: Fishbrain[]) {
    for (let it in allbrains) {
      if (allbrains[it] instanceof Fuzzybrain) {
        this.brainSubject.next(allbrains[it] as Fuzzybrain);
      }
    }
  }
  reportStringChange() {
    if(this.focusedBrain) {
      this.focusedBrain.applyVarStrings(this.alphaQueryString,
      this.bravoQueryString, this.charlieQueryString);
      this.focusedBrain.applyDecideStrings(this.romeoDecideString,
      this.sierraDecideString, this.tangoDecideString);
    }
  }
  /** Receive data of the fish body and transmit message if it has changed */
  reportFishBody(fish: Fishbody) {
    this.focusedSize = fish.size;
    let change = this.focusedBody.clone(fish);
    if (change) {
      this.bodySubject.next(this.focusedBody);
    }
  }
  /** Receive data of the every body and every brain, then send message if it is time to tick */
  reportAllBodies(millis: number, allfish: Fishbody[], allbrains: Fishbrain[]) {
    for (let it in allbrains) {
      if (allbrains[it].selfbody != null && allbrains[it].checkVar(millis)) {
        /** update var will change the alpha beta gamma bar graph values */
        allbrains[it].updateVar(allfish);
        if (allbrains[it] instanceof Fuzzybrain) {
          this.focusedBrain = allbrains[it]  as Fuzzybrain;
          this.brainSubject.next(this.focusedBrain);
        }
      }
      if (allbrains[it].selfbody != null && allbrains[it].checkDecision(millis)) {

        /** update decision will create a new destination */
        let result = allbrains[it].updateDecision(allfish, 
          this.romeoEnabled,this.romeoAlphaRelation,
          this.romeoBravoRelation, this.romeoCharlieRelation,
          this.sierraEnabled, this.sierraAlphaRelation, 
          this.sierraBravoRelation, this.sierraCharlieRelation, 
          this.tangoEnabled, this.tangoAlphaRelation,
          this.tangoBravoRelation, this.tangoCharlieRelation);

        if (allbrains[it] instanceof Fuzzybrain) {
          if (result == 'r') {
            this.romeoExecuted += 1;
          }
          if (result == 's') {
            this.sierraExecuted += 1;
          }
          if (result == 't') {
            this.tangoExecuted += 1;
          }
          this.brainSubject.next(allbrains[it] as Fuzzybrain);
        }
      }
    }
  }
  /** clean up some global fields for when teh focused fish has died */
  reportDeath(size: number) {
    this.romeoExecuted = 0;
    this.sierraExecuted = 0;
    this.tangoExecuted = 0;
    this.bestSize = Math.max(this.bestSize, size);
    this.focusedSize = 0;
    this.humanMode = false;
  }

  /** Toggle some angular panels */
  toggleView(): void {
    this.showScrip = !this.showScrip;
    this.showVartune = !this.showScrip;
  }
  /** Toggle editing of the script */
  toggleHuman(): void {
    this.humanMode = !this.humanMode;
  }
  loadSmartQueries(): void {
    this.romeoEnabled = true;
    this.sierraEnabled = true;
    this.tangoEnabled = true;
    if(this.focusedBrain) {
      this.focusedBrain.loadSmartQueries();
      this.brainSubject.next(this.focusedBrain);
    }
  }
  loadBasicQueries(): void {
    this.sierraEnabled = false;
    this.tangoEnabled = false;
    if(this.focusedBrain) {
      this.focusedBrain.loadBasicQueries();
      this.brainSubject.next(this.focusedBrain);
    }
  }
}
