/** this is the base class for some very basic movement */
import { Fishbody } from './fishbody'
import { FuzzyLib } from './fuzzylib'

export class Fishbrain {

    public selfbody:Fishbody;
    public decisionX:number = 0;
    public decisionY:number = 0;
    public decisionHeld:boolean = false;

    private changeVar:number;
    private changeDecision:number;
    protected changeDecisionMax:number;
    constructor() {
        this.changeVar = 300;
        this.changeDecisionMax = 3000+Math.random()*600;
        this.changeDecision = this.changeDecisionMax;
    }
    
    public handleSpawn(x:number, y:number) {
        this.decisionX = x/100 + 1;
        this.decisionY = y/100;
    }

    public checkVar(millis:number):boolean {
        this.changeVar -= millis;
        if(this.changeVar < 0) {
            this.changeVar = 300;
            return true;
        }
        return false;
    }
    public checkDecision(millis:number):boolean {
        this.changeDecision -= millis;
        if(this.changeDecision < 0) {
            this.changeDecision = this.changeDecisionMax;
            return true;
        }
        return false;
    }


    public updateVar(allbodies:Fishbody[]) {

    }

    public updateDecision(allbodies:Fishbody[], hasr, ra, rb, rc, hass, sa, sb, sc, hast, ta, tb, tc):string {
        let fallback:number[] = [0,1,0];

        let decideValue = FuzzyLib.randomLocation(this.selfbody, null);
        this.decisionX = decideValue[0];
        this.decisionY = decideValue[1];
        this.decisionHeld = decideValue[2] > 0;
        return ' ';
    }


}
