/** This component has the data which moves the body forward, or in an alternate method */
export enum Movement {
    Spawning,
    Free,
    Attached,
    Food,
    Dead,
}

export class Fishphysics {
    public id:number;
    public pph:Fishphysics;
    public children:number[] = [];
    public desiredVelX:number = 0;
    public desiredVelY:number = 0;
    public velx:number = 0;
    public vely:number = 0;
    public x:number;
    public y:number;
    public dirx:number = 1;
    public diry:number = 0;
    public size:number = 1;
    public sprint:boolean = false;
    public movement:Movement;
    public lifetime:number = 0;
    public ttl:number = 9999999;
    public sprintSpeed:number = 4500;
    public coastSpeed:number = 2000;

    constructor(id:number, x:number, y:number) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.movement = Movement.Spawning;
    }

    setMovement(next:Movement) {
        if(next == this.movement) {
            return;
        }
        this.movement = next;
        if(this.movement == Movement.Attached) {
            this.velx = 0;
            this.vely = 0;
        }
        if(this.movement == Movement.Dead) {
            this.pph = null;
        }
    }


    tick(millis:number) {
        this.lifetime += millis;
        this.ttl -= millis;
        if(this.movement == Movement.Food ||
            this.movement == Movement.Dead)
        {
            return;
        }
        switch(this.movement)
        {
            case Movement.Spawning:
            {
                if(this.lifetime > 1000) {
                    this.setMovement(Movement.Free);
                }
                break;
            }
            case Movement.Free:
            {
                this.velx = this.desiredVelX;
                this.vely = this.desiredVelY;
                break;
            }
            default:
            {
                break;
            }
        }
        if(this.movement == Movement.Attached) {
            /** snap to the parents coordinate with an offset */
            this.x = this.pph.x + this.pph.dirx*this.pph.size*2;
            this.y = this.pph.y + this.pph.diry*this.pph.size*2;
            this.dirx = -this.pph.diry;
            this.diry = this.pph.dirx;
        } else {
            let deltax = this.velx*millis/1000;
            if(Math.abs(deltax) > 0) {
                this.x += deltax;
            }
            let deltay = this.vely*millis/1000;
            if(Math.abs(deltay) > 0) {
                this.y += deltay;
            }
        }
    }
}
