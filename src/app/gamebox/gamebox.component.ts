import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { BridgeService } from '../bridge.service'
import { Fishbody } from '../fishbody'
import * as PIXI from 'pixi.js';
import * as BOSSES from '../bosses';
import { Controller, BrainController, HumanController } from '../controllers';

@Component({
  selector: 'gamebox',
  templateUrl: './gamebox.component.html',
  styleUrls: ['./gamebox.component.css']
})


export class GameboxComponent implements OnInit {

  @ViewChild("pixi")
  public pixiContainer: ElementRef;
  private pixi:PIXI.Application;
  public boss:BOSSES.Boss;
  public localHuman:HumanController;
  public focusedController:BrainController;
  public lastFocusedID:number = 0;
  public lastFocusedSize:number = 0;
  public canvas:HTMLCanvasElement;
  private laststamp:number = 0;
  constructor(public bridge: BridgeService) {
    
  }

  tick(timestamp) {
    let millis = Math.min(timestamp - this.laststamp,500);
    this.laststamp = timestamp;
    this.bridge.tick(millis);
    this.boss.tick(millis);
    this.retrieve();
    requestAnimationFrame(this.tick.bind(this));
    this.expose(millis);
    
    
  }

  ngOnInit() {
    this.canvas = document.getElementById('gamecanvas') as HTMLCanvasElement;
    this.pixi = new PIXI.Application(1024,768,{
      backgroundColor:0x112266, antialias: true, view:this.canvas
    });
    let aspectRatio = (this.pixi.renderer.height / this.pixi.renderer.width);
    this.pixi.view.style.width = "100%";
    this.pixi.view.style.height = `${aspectRatio*100}%`;

    this.pixiContainer.nativeElement.appendChild(this.pixi.view);

    this.localHuman = new HumanController(this.canvas); 
  

    var bgtex = PIXI.Texture.fromImage('assets/bg.jpg');
    let bg = new PIXI.Sprite(bgtex);
    bg.scale.set(this.pixi.renderer.height/1200);
    this.pixi.stage.addChild(bg);
    
    this.boss = new BOSSES.Boss(this.pixi.stage, this.pixi.renderer, this.canvas);
    this.boss.launch();
    this.bridge.firstReport(this.boss.brains);
    requestAnimationFrame(this.tick.bind(this));
  }

  getFocusFishID() : number {
    if(this.focusedController != null && 
    this.focusedController.fish != null)
    {
      return this.focusedController.fish.id;
    }
  }
  expose(millis:number) {
    if(this.getFocusFishID() != this.lastFocusedID) {
      // the fish changed
      if(this.lastFocusedID > 0) {
        this.bridge.reportDeath(this.lastFocusedSize);
      }
      this.focusedController = null;
    }

    if(this.focusedController == null) {
      this.boss.tick(0);
      this.focusedController = this.boss.findFirstBrain();
      this.focusedController.fish.setFocused(true);
      this.lastFocusedID = this.focusedController.fish.id;
    }

    if(this.focusedController != null && 
      this.focusedController.fish != null)
    {
      // there is a fish, so report it
      this.bridge.reportFishBody(this.focusedController.fish.body);
      this.lastFocusedSize = this.focusedController.fish.body.size;
    }
    this.bridge.reportAllBodies(millis, this.boss.bodies, this.boss.brains);
  }

  retrieve() {
    if(this.bridge.humanMode) {
      if(this.focusedController.fish != this.localHuman.fish) {
        this.localHuman.fish = this.focusedController.fish;
        this.focusedController.thinking = false;
      }
    } else {
      if(this.localHuman.fish != null) {
        this.localHuman.fish = null;
        this.focusedController.thinking = true;
      }
    }
  }
}
