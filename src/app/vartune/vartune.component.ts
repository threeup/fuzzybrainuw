import { Component, OnInit } from '@angular/core';
import { BridgeService } from '../bridge.service'

@Component({
  selector: 'vartune',
  templateUrl: './vartune.component.html',
  styleUrls: ['./vartune.component.css']
})
export class VartuneComponent implements OnInit {
  public name:string;
  public alphaValue:number = 13;
  public bravoValue:number = 3;
  public charlieValue:number = 10;
  public alphaContents:string = "";
  public bravoContents:string = "";
  public charlieContents:string = "";
  public tickInterval:number = 1;
  mode = 'determinate';
  dense = true;
  bufferValue = 11;
  constructor(public bridge: BridgeService) { 

    bridge.brainSubject.subscribe(fuzzybrain => {
      this.alphaValue = fuzzybrain.alphaValue;
      this.bravoValue = fuzzybrain.bravoValue;
      this.charlieValue = fuzzybrain.charlieValue;
      if(!bridge.editMode) {
        this.alphaContents = fuzzybrain.alphaQueryString;
        this.bravoContents = fuzzybrain.bravoQueryString;
        this.charlieContents = fuzzybrain.charlieQueryString;
      }
    });

  }

  ngOnInit() {
  }
  alphaChange(event) {
    this.alphaContents = event;
    this.bridge.alphaQueryString = this.alphaContents;
  }
  bravoChange(event) {
    this.bravoContents = event;
    this.bridge.bravoQueryString = this.bravoContents;
  }
  charlieChange(event) {
    this.charlieContents = event;
    this.bridge.charlieQueryString = this.charlieContents;
  }
  insertAlpha() {
    if(this.alphaContents == null || this.alphaContents.length == 0) {
      this.alphaContents = "FuzzyQuery"
    }
    this.alphaContents += "."+this.bridge.funcString+"("+this.bridge.paramString+")";
  }
  insertBravo() {
    if(this.bravoContents == null || this.bravoContents.length == 0) {
      this.bravoContents = "FuzzyQuery"
    }
    this.bravoContents += "."+this.bridge.funcString+"("+this.bridge.paramString+")";
  }
  insertCharlie() {
    if(this.charlieContents == null || this.charlieContents.length == 0) {
      this.charlieContents = "FuzzyQuery"
    }
    this.charlieContents += "."+this.bridge.funcString+"("+this.bridge.paramString+")";
  }
}
