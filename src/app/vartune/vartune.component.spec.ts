import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VartuneComponent } from './vartune.component';

describe('VartuneComponent', () => {
  let component: VartuneComponent;
  let fixture: ComponentFixture<VartuneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VartuneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VartuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
