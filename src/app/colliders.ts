/** the minimal amount of data needed for sphere collision detection, uses id to reassocate to fish  */

import { Fishphysics } from './fishphysics'
export class Collider {


    public id: number;
    public x: number;
    public y: number;
    public r: number;
    public enabled: boolean;
    public ph: Fishphysics;
    constructor(id: number, ph: Fishphysics) {
        this.id = id;
        this.r = 0;
        this.ph = ph;
        this.x = ph.x;
        this.y = ph.y;
        this.enabled = true;
    }


}