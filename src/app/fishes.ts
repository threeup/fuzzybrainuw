/** Fish is an entire entity comprised of components for gameplay, ai, and rendering */
import * as PIXI from 'pixi.js';
import * as COLLIDERS from './colliders'
import { Fishbody } from './fishbody'
import { Fishbrain } from './fishbrain'
import { Fishphysics, Movement } from './fishphysics'



export class Fish {
    public id:number;
    public body:Fishbody;
    public brain:Fishbrain;
    public ph:Fishphysics;
    public collider:COLLIDERS.Collider;

    public parent:Fish;
    public spriteBase:PIXI.Sprite;
    public spriteColor:PIXI.Sprite;
    public focused:boolean = false;
    public food:boolean = false;
    public tint:string = "0xffffff";
    private lastSize:number = 0;
    
    constructor(id:number, size:number, 
        spriteBase:PIXI.Sprite, spriteColor:PIXI.Sprite, realX:number, realY:number) {
        this.id = id;
        this.parent = null;
        this.spriteBase = spriteBase;
        this.spriteBase.x = realX/100;
        this.spriteBase.y = realY/100;
        this.spriteColor = spriteColor;
        this.spriteColor.x = realX/100;
        this.spriteColor.y = realY/100;
        
        this.body = new Fishbody(id, size);
        this.ph = new Fishphysics(id, realX, realY);
        this.collider = new COLLIDERS.Collider(id, this.ph);     
        this.setFocused(false);
        this.tick(0);  
    }
    //** returns true when hp got low, or the expiration check came */
    isDead() : boolean {
        return this.body.hp <= 0 || this.ph.ttl < 0;
    }
    setTint(tint:string) {
        this.tint = tint.length > 0 ? tint : "0x"+(Math.random()*0xFFFFFF<<0).toString(16);
        this.spriteColor.tint = parseInt(this.tint);
    }
    setFocused(focused:boolean) {
        this.focused = focused;
        if(focused) {
            this.spriteBase.tint = 0x00ff00;
        } else {
            this.spriteBase.tint = 0xaaaaaa;
        }
    }

    tick(millis:number) {
        this.ph.tick(millis);
        this.collider.x = Math.round(this.ph.x);
        this.collider.y = Math.round(this.ph.y);
        /** this is a bit clumsy, syncronizing the size and radius of all the components */
        if(this.body.size != this.lastSize) {
            this.spriteBase.scale.set(0.03*this.body.size);
            this.spriteColor.scale.set(0.03*this.body.size);
            this.ph.size = this.body.size*200;
            this.collider.r = this.ph.size;
            this.lastSize = this.body.size;
            
        }
        if(this.ph.movement == Movement.Free){
            if(this.ph.sprint) {
                /** how fast energy depletes when sprinting */
                let rate = 0.6;
                this.body.energy = Math.max(this.body.energy - millis*rate, 0);
            } else {
                /** how fast energy gains when not sprinting */
                let rate = 0.25;
                this.body.energy = Math.min(this.body.energy + millis*rate, this.body.maxenergy);
            }
    
            if(this.ph.children.length > 0) {
                /** how fast hunger subsides when eating */
                let rate = 0.3*this.ph.children.length;
                this.body.hunger = Math.max(this.body.hunger - millis*rate, 0);
            } else {
                /** how fast hunger grows when not eating */
                let rate = 0.03+0.005*this.lastSize;
                this.body.hunger = Math.min(this.body.hunger + millis*rate, this.body.maxhunger);
            }
        }
        if(this.ph.movement == Movement.Attached) {
            /** how fast to die when being eaten */
            let rate = 0.2;
            this.body.hp = Math.max(this.body.hp - millis*rate, 0);
        }
        if(this.body.hunger >= this.body.maxhunger) {
            /** how fast to die hungry */
            let rate = 1.5;
            this.body.hp = Math.max(this.body.hp - millis*rate, 0);
        }

        this.body.x = this.ph.x/100;
        this.body.y = this.ph.y/100;
        this.spriteBase.x = this.body.x;
        this.spriteBase.y = this.body.y;
        this.spriteBase.rotation = Math.PI+Math.atan2(this.ph.diry, this.ph.dirx);
        this.spriteColor.x = this.body.x;
        this.spriteColor.y = this.body.y;
        this.spriteColor.rotation = this.spriteBase.rotation;
        
    }


    inputAction(held:boolean):void {
        if(this.ph.sprint) {
            /** dont sprint if there is no energy */
            if (!held || this.body.energy < 1) {
                this.ph.sprint = false;
                
            }
        } else {
            /** only start sprinting if there is 500 energy */
            if(held && this.body.energy > 500) {
                this.ph.sprint = true;
            }
        }
        this.ph.desiredVelX = this.ph.dirx * (this.ph.sprint?this.ph.sprintSpeed:this.ph.coastSpeed);
        this.ph.desiredVelY = this.ph.diry * (this.ph.sprint?this.ph.sprintSpeed:this.ph.coastSpeed);
    }
    inputLocation(x:number, y:number):void {
        /** clamp inputs to some boundaries */
        x = Math.max(50,x);
        y = Math.max(50,y);
        x = Math.min(1000,x);
        y = Math.min(750,y);
        let deltax = x - this.body.x;
        let deltay = y - this.body.y;
        let deltalen = Math.sqrt(deltax*deltax+deltay*deltay);
        if(deltalen > 9) {
            /** only change direction if the difference is significant */
            this.ph.dirx = deltax/deltalen;
            this.ph.diry = deltay/deltalen;
        }
    }
}