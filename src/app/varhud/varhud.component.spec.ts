import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarhudComponent } from './varhud.component';

describe('VarhudComponent', () => {
  let component: VarhudComponent;
  let fixture: ComponentFixture<VarhudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarhudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarhudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
