import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { BridgeService } from '../bridge.service'

@Component({
  selector: 'varhud',
  templateUrl: './varhud.component.html',
  styleUrls: ['./varhud.component.css']
})
export class VarhudComponent implements OnInit {
  public alphaValue:number = 63;
  public bravoValue:number = 99;
  public charlieValue:number = 30;
  constructor(public bridge: BridgeService) { 
    bridge.brainSubject.subscribe(fuzzybrain => {
      this.alphaValue = 30+fuzzybrain.alphaValue*7;
      this.bravoValue = 30+fuzzybrain.bravoValue*7;
      this.charlieValue = 30+fuzzybrain.charlieValue*7;
    });
  }

  ngOnInit() {
  }

}
