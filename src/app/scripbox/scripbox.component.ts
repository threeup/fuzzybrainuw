import { Component, OnInit } from '@angular/core';
import { BridgeService } from '../bridge.service'

@Component({
  selector: 'scripbox',
  templateUrl: './scripbox.component.html',
  styleUrls: ['./scripbox.component.css']
})
export class ScripboxComponent implements OnInit {

  public scrip:string;

    dense = true;
    romeoContents: string;
    sierraContents: string;
    tangoContents: string;
    romeoExecuted: number = 0;
    sierraExecuted: number = 0;
    tangoExecuted: number = 0;
    romeoAlpha = 0;
    romeoBravo = 0;
    romeoCharlie = 0;
    sierraAlpha = 0;
    sierraBravo = 0;
    sierraCharlie = 0;
    tangoAlpha = 1;
    tangoBravo = 0;
    tangoCharlie = 0;
  constructor(public bridge: BridgeService) 
  { 
    this.romeoAlpha = bridge.romeoAlphaRelation;
    this.romeoBravo = bridge.romeoBravoRelation;
    this.romeoCharlie = bridge.romeoCharlieRelation;
    this.sierraAlpha = bridge.sierraAlphaRelation;
    this.sierraBravo = bridge.sierraBravoRelation;
    this.sierraCharlie = bridge.sierraCharlieRelation;
    this.tangoAlpha = bridge.tangoAlphaRelation;
    this.tangoBravo = bridge.tangoBravoRelation;
    this.tangoCharlie = bridge.tangoCharlieRelation;
    bridge.brainSubject.subscribe(fuzzybrain => {
      if(!bridge.editMode) {
        this.romeoContents = fuzzybrain.romeoDecideString;
        this.sierraContents = fuzzybrain.sierraDecideString;
        this.tangoContents = fuzzybrain.tangoDecideString;
      }
      this.romeoExecuted = bridge.romeoExecuted;
      this.sierraExecuted = bridge.sierraExecuted;
      this.tangoExecuted = bridge.tangoExecuted;
    });
    
  }

  ngOnInit() {
  }
  romeoChange(event) {
    this.romeoContents = event;
    this.bridge.romeoDecideString = this.romeoContents;
  }
  sierraChange(event) {
    this.sierraContents = event;
    this.bridge.sierraDecideString = this.sierraContents;
  }
  tangoChange(event) {
    this.tangoContents = event;
    this.bridge.tangoDecideString = this.tangoContents;
  }
  insertRomeo() {
    if(!this.bridge.editMode) {
      this.bridge.editMode = true;
    }
    if(this.romeoContents == null || this.romeoContents.length == 0) {
      this.romeoContents = "FuzzyQuery"
    }
    this.romeoContents += "."+this.bridge.funcString+"("+this.bridge.paramString+")";
  }
  insertSierra() {
    if(!this.bridge.editMode) {
      this.bridge.editMode = true;
    }
    if(this.sierraContents == null || this.sierraContents.length == 0) {
      this.sierraContents = "FuzzyQuery"
    }
    this.sierraContents += "."+this.bridge.funcString+"("+this.bridge.paramString+")";
  }
  insertTango() {
    if(!this.bridge.editMode) {
      this.bridge.editMode = true;
    }
    if(this.tangoContents == null || this.tangoContents.length == 0) {
      this.tangoContents = "FuzzyQuery"
    }
    this.tangoContents += "."+this.bridge.funcString+"("+this.bridge.paramString+")";
  }

  romeoAlphaChange(event) {
    this.bridge.romeoAlphaRelation = event.value;
  }
  romeoBravoChange(event) {
    this.bridge.romeoBravoRelation = event.value;
  }
  romeoCharlieChange(event) {
    this.bridge.romeoCharlieRelation = event.value;
  }
  sierraAlphaChange(event) {
    this.bridge.sierraAlphaRelation = event.value;
  }
  sierraBravoChange(event) {
    this.bridge.sierraBravoRelation = event.value;
  }
  sierraCharlieChange(event) {
    this.bridge.sierraCharlieRelation = event.value;
  }
  tangoAlphaChange(event) {
    this.bridge.tangoAlphaRelation = event.value;
  }
  tangoBravoChange(event) {
    this.bridge.tangoBravoRelation = event.value;
  }
  tangoCharlieChange(event) {
    this.bridge.tangoCharlieRelation = event.value;
  }
}
