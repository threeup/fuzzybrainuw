import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScripboxComponent } from './scripbox.component';

describe('ScripboxComponent', () => {
  let component: ScripboxComponent;
  let fixture: ComponentFixture<ScripboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScripboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScripboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
