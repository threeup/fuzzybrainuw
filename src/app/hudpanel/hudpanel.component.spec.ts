import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HudpanelComponent } from './hudpanel.component';

describe('HudpanelComponent', () => {
  let component: HudpanelComponent;
  let fixture: ComponentFixture<HudpanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HudpanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HudpanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
