import { Component, OnInit } from '@angular/core';
import { BridgeService } from '../bridge.service'

@Component({
  selector: 'hudpanel',
  templateUrl: './hudpanel.component.html',
  styleUrls: ['./hudpanel.component.css']
})
export class HudpanelComponent implements OnInit {
  mode = 'determinate';
  color = 'primary';
  buffer = 10;
  bufferValue = 100;
  size = 1;
  hp = 1;
  energy = 1;
  hunger = 1;
  constructor(public bridge: BridgeService) { 
    bridge.bodySubject.subscribe(fishbody => {
      this.size = 100*fishbody.size/20;
      this.hp = 100*fishbody.hp/1000;
      this.energy = 100*fishbody.energy/fishbody.maxenergy;
      this.hunger = 100-100*fishbody.hunger/fishbody.maxhunger;
    });
  }

  ngOnInit() {
  }

}
