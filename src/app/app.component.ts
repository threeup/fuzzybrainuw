import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { BridgeService } from './bridge.service'
import { Fishbody } from './fishbody'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title:string = 'Fuzzy Brain';
  subtitle:string = 'Sheridan Thirsk CSEP504 University of Washington';
  activetab = 0;
  disabled = false;
  activate(index: number) {
    this.activetab = index;
  }
  constructor(public bridge: BridgeService, 
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
      iconRegistry.addSvgIcon(
        'gralpha',
        sanitizer.bypassSecurityTrustResourceUrl('assets/alpha.svg'));
      iconRegistry.addSvgIcon(
        'grbeta',
        sanitizer.bypassSecurityTrustResourceUrl('assets/beta.svg'));
      iconRegistry.addSvgIcon(
        'grgamma',
        sanitizer.bypassSecurityTrustResourceUrl('assets/gamma.svg'));
      iconRegistry.addSvgIcon(
        'grmu',
        sanitizer.bypassSecurityTrustResourceUrl('assets/mu.svg'));
      iconRegistry.addSvgIcon(
        'grphi',
        sanitizer.bypassSecurityTrustResourceUrl('assets/phi.svg'));
      iconRegistry.addSvgIcon(
        'grrho',
        sanitizer.bypassSecurityTrustResourceUrl('assets/rho.svg'));
  }
  switchEdit() {
    this.bridge.reportStringChange();
  }
  loadSmart() {
    this.bridge.loadSmartQueries();
  }
  loadBasic() {
    this.bridge.loadBasicQueries();
  }
}

