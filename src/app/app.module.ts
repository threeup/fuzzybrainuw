import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@blox/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { BridgeService } from './bridge.service';
import { GameboxComponent } from './gamebox/gamebox.component';
import { VartuneComponent } from './vartune/vartune.component';
import { ScripboxComponent } from './scripbox/scripbox.component';
import { HudpanelComponent } from './hudpanel/hudpanel.component';
import { HelperComponent } from './helper/helper.component';
import { VarhudComponent } from './varhud/varhud.component';


@NgModule({
  declarations: [
    AppComponent,
    GameboxComponent,
    VartuneComponent,
    ScripboxComponent,
    HudpanelComponent,
    HelperComponent,
    VarhudComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    MaterialModule,
    MatGridListModule,
    MatTabsModule,
    MatProgressBarModule,
    MatIconModule,
    MatSliderModule,
    MatExpansionModule,
    MatToolbarModule,
    MatChipsModule,
    MatCheckboxModule,
  ],
  providers: [BridgeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
