/** A controller takes decisions from the human or brains and sends signals to control the fish */

import * as PIXI from 'pixi.js';
import { Fishbody } from './fishbody'
import { Fishbrain } from './fishbrain'
import * as FISHES from './fishes'

/** base empty class */
export class Controller {
    public fish:FISHES.Fish;
    constructor() {
        
    }
    tick(millis:number) {
        if(this.fish) {
            this.fish.tick(millis);
        }
    }
    spawn(x:number, y:number) {

    }
    inputLocation(mx:number, my:number):void {
        if(this.fish) {
            this.fish.inputLocation(mx, my);
        }
    }
    inputAction(held:boolean):void {
        if(this.fish) {
            this.fish.inputAction(held);
        }
    }
}

/** Humans listen to the document, throw out events if the target doesnt match the canvas */
export class HumanController extends Controller{
    private isDown:boolean;
    private canvas:HTMLCanvasElement;
    constructor(canvas:HTMLElement) {
        super();
        this.canvas = canvas as HTMLCanvasElement;
        document.addEventListener('touchstart', this.onClickDown.bind(this), true)
        document.addEventListener('touchmove', this.onClickMove.bind(this), true)
        document.addEventListener('touchend', this.onClickUp.bind(this), true)
      
        document.addEventListener('mousedown', this.onClickDown.bind(this), true)
        document.addEventListener('mousemove', this.onClickMove.bind(this), true)
        document.addEventListener('mouseup', this.onClickUp.bind(this), true)
    }
    tick(millis:number) {
        super.tick(millis);
    }

    onClickDown(e):void {
        if(e.target == this.canvas) {
            this.adjustedInput(e.offsetX, e.offsetY);
            this.isDown = true;
            this.inputAction(this.isDown);
        }
    }
    onClickMove(e):void {
        if(e.target == this.canvas) {
            if(this.isDown) {
                this.adjustedInput(e.offsetX, e.offsetY);
            }
            this.inputAction(this.isDown);
        }
    }
    onClickUp(e):void {
        if(e.target == this.canvas) {
            this.adjustedInput(e.offsetX, e.offsetY);
            this.isDown = false;
            this.inputAction(this.isDown);
        }
    }
    adjustedInput(mx:number, my:number):void {
        this.inputLocation(mx*this.canvas.width/this.canvas.clientWidth, 
                my*this.canvas.height/this.canvas.clientHeight);
    }

}

/** Brain Controllers listen to the brain */
export class BrainController extends Controller{
    public brain:Fishbrain;
    public body:Fishbody;
    public nearest:Fishbody[];
    public thinking:boolean = true;
    constructor() {
        super();
    }
    tick(millis:number) {
        if(this.thinking) {
            this.inputLocation(this.brain.decisionX, this.brain.decisionY);
            this.inputAction(this.brain.decisionHeld);
        }
        
        super.tick(millis);
    }
    spawn(x:number, y:number) {
        this.brain.handleSpawn(x,y);
        super.spawn(x,y);
    }
}