/** An intelligent class which implements the FuzzyBrain language */
import { Fishbody } from './fishbody'
import { Fishbrain } from './fishbrain'
import { FuzzyLib } from './fuzzylib'
import { BrainFiltering, BrainScoring, FuzzyQuery } from './fuzzyquery'

export class Fuzzybrain extends Fishbrain {


    public alphaValue:number = 0;
    public bravoValue:number = 0;
    public charlieValue:number = 0;

    public alphaQueryString:string;
    public bravoQueryString:string;
    public charlieQueryString:string;
    public romeoDecideString:string;
    public sierraDecideString:string;
    public tangoDecideString:string;
    private alphaQuery:FuzzyQuery;
    private bravoQuery:FuzzyQuery;
    private charlieQuery:FuzzyQuery;
    private decideQuery:FuzzyQuery;
    private romeoQuery:FuzzyQuery;
    private sierraQuery:FuzzyQuery;
    private tangoQuery:FuzzyQuery;
    constructor() {
        super();
        this.changeDecisionMax = 1000;
        this.loadBasicQueries();
    }

    public loadBasicQueries() {
        this.alphaQueryString = "Query.sort(sortingDistance).map(scoringDistance)";
        this.bravoQueryString = "Query.sort(sortingDistance).map(scoringDistance)"; 
        this.charlieQueryString = "Query.sort(sortingDistance).map(scoringDistance)";
        this.romeoDecideString = "Query.map(randomLocation)";
        this.sierraDecideString = "Query.map(randomLocation)";
        this.tangoDecideString = "Query.map(randomLocation)";
    }

    public loadSmartQueries() {
        this.alphaQueryString = "Query.filter(filterFood).\
        filter(filterCanEat).sort(sortingDistance).max(one).map(scoringDistance)";
        this.bravoQueryString = "Query.filter(filterLiving).\
        filter(filterCanEat).sort(sortingDistance).max(one).map(scoringDistance)";
        this.charlieQueryString = "Query.filter(filterLiving).\
        filter(filterCanBeEaten).sort(sortingDistance).max(one).map(scoringDistance)";
        this.romeoDecideString = "Query.filter(filterFood).\
        filter(filterCanEat).sort(sortingDistance).max(one).map(towardLocation)";
        this.sierraDecideString = "Query.filter(filterLiving).\
        filter(filterCanEat).sort(sortingDistance).max(one).map(towardLocation)";
        this.tangoDecideString = "Query.filter(filterLiving).\
        filter(filterCanBeEaten).sort(sortingDistance).max(one).map(awayFromLocation)";
    }
   
    public getFilter(self:Fishbody, all:Fishbody[], filtering:BrainFiltering):Fishbody[] {
        let result:Fishbody[] = [];

        for(let it in all) {
            if(filtering(self,all[it])) {
                result.push(all[it])
            }
        }
        return result;
    }
    public getHighestScore(self:Fishbody, subset:Fishbody[], scoring:BrainScoring):number {
        let bestScore = -1;
        for(let it in subset) {
            let score = scoring(self,subset[it]);
            if(score > bestScore) {
                bestScore = score;
            }
        }
        return bestScore;
    }
    /** take the queries from the angular interface */
    public applyVarStrings(aqs:string, bqs:string, cqs:string) {
        if(aqs && aqs.length > 0){this.alphaQueryString = aqs;}
        if(bqs && bqs.length > 0){this.bravoQueryString = bqs;}
        if(cqs && cqs.length > 0){this.charlieQueryString = cqs;}
    }
    /** take the queries from the angular interface */
    public applyDecideStrings(rds:string, sds:string, tds:string) {
        if(rds && rds.length > 0) {this.romeoDecideString = rds;}
        if(sds && sds.length > 0) {this.sierraDecideString = sds;}
        if(tds && tds.length > 0) {this.tangoDecideString = tds;}
    }

    /** process the queries to compute alpha beta gamma variable values */
    public updateVar(allbodies:Fishbody[]) {

        let aq = this.safeParse(this.alphaQueryString)
        let aqget = aq.get(this.selfbody,allbodies);
        this.alphaValue = aqget.length > 0 ? aqget[0] as number : -1;
        let bq = this.safeParse(this.bravoQueryString)
        let bqget = bq.get(this.selfbody,allbodies);
        this.bravoValue = bqget.length > 0 ? bqget[0] as number : -1;
        let cq = this.safeParse(this.charlieQueryString)
        let cqget = cq.get(this.selfbody,allbodies);
        this.charlieValue = cqget.length > 0 ? cqget[0] as number : -1;
    }
    /** return a sortable value between relation and the current values, this is used for deciding which relation is best */
    public relationSq(ra:number, rb:number, rc:number, a:number, b:number, c:number):number {
        let da = (5+ra*5)-a;
        let db = (5+rb*5)-b;
        let dc = (5+rc*5)-c;
        return da*da+db*db+dc*dc;
    }
    /** decide which relation to use, and then use that decision to send inputs to the fish */
    public updateDecision(allbodies:Fishbody[], hasr:boolean, ra, rb, rc, hass:boolean, sa, sb, sc, hast:boolean, ta, tb, tc):string {
        let fallback:number[] = [0,1,0];
        let result = ' '
        let romeoRelationSq = hasr ? this.relationSq(ra, rb, rc, 
            this.alphaValue, this.bravoValue, this.charlieValue) : 99999999;
        let sierraRelationSq = hass ? this.relationSq(sa, sb, sc, 
            this.alphaValue, this.bravoValue, this.charlieValue) : 99999999;
        let tangoRelationSq = hast ? this.relationSq(ta, tb, tc, 
            this.alphaValue, this.bravoValue, this.charlieValue) : 99999999;
        let query = null;
        if(romeoRelationSq < sierraRelationSq && romeoRelationSq < tangoRelationSq)
        {
            query = this.safeParse(this.romeoDecideString);
            result = 'r';
        }
        else if(sierraRelationSq < tangoRelationSq)
        {
            query = this.safeParse(this.sierraDecideString);
            result = 's';
        }
        else
        {
            query = this.safeParse(this.tangoDecideString);
            result = 't';
        }

        let queryGet = query.get(this.selfbody,allbodies);
        let decideValue = queryGet.length > 0 ? queryGet[0] as number[] : fallback;
        this.decisionX = decideValue[0];
        this.decisionY = decideValue[1];
        this.decisionHeld = decideValue[2] > 0;
        return result;
    }

    /** parse the text inputted by the user */
    public safeParse(blob:String):FuzzyQuery {
        let chunks = blob.split('.');
        let result:FuzzyQuery = new FuzzyQuery();
        for(let x = 1; x<chunks.length; ++x){
            let args = chunks[x].trim().split(/[()]/);
            let method = FuzzyQuery.prototype[args[0]];
            let param = FuzzyLib[args[1]];
            if(method != null && param != null) {
                result = method.call(result,param);
            } else {
                console.log("failed to parse"+chunks[x]);
            }
            
        }
        return result;

    }
}
