/** functions in the fuzzy brain library */
import { Fishbody } from './fishbody'
import { map } from 'rxjs/operator/map';

export class FuzzyLib {
    /** returns true if it is food */
    static filterFood(self:Fishbody,other:Fishbody):boolean {
        return other.food;
    }
    /** returns true if it is a living fish, not food */
    static filterLiving(self:Fishbody,other:Fishbody):boolean {
        return !other.food;
    }
    /** returns true if the other entity is within the size constraints of food */
    static filterCanEat(self:Fishbody,other:Fishbody):boolean {
        let sizeDifference = self.size - other.size;
        return sizeDifference >= 1 && sizeDifference <= 5;
    }
    /** returns true if the self entity is within the size constraints of food for the other entity */
    static filterCanBeEaten(self:Fishbody,other:Fishbody):boolean {
        let sizeDifference = other.size - self.size;
        return sizeDifference >= 1 && sizeDifference <= 5;
    }
    /** returns a number which represents distance, higher is closer */
    static scoringDistance(self:Fishbody,other:Fishbody):number {
        let deltaDifferenceSq = FuzzyLib.distSq(other.x,other.y, self.x, self.y);
        return FuzzyLib.scoreFromSq(deltaDifferenceSq);
    }
    /** returns a sorting value 1 or -1 for distance comparision */
    static sortingDistance(self:Fishbody,lhs:Fishbody,rhs:Fishbody):number {
        let lhsDifferenceSq = FuzzyLib.distSq(lhs.x, lhs.y, self.x, self.y);
        let rhsDifferenceSq = FuzzyLib.distSq(rhs.x, rhs.y, self.x, self.y);
        return lhsDifferenceSq > rhsDifferenceSq ? 1 : -1;
    }
    /** math helper for distance squared */
    static distSq(x1,y1,x2,y2): number{ 
        return ((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); 
    }
    /** math helper for scoring distance squared */
    static scoreFromSq(num:number):number{
        if(num > 720*720) {
            return 1;
        } else if(num > 480*480) {
            return 3;
        } else if(num > 240*240) {
            return 5;
        } else if(num > 120*120) {
            return 7;
        } else if(num > 60*60) {
            return 9;
        }
        return 10;
    }
    /** returns a set of 3 inputs representing random location */
    static randomLocation(self:Fishbody,_:Fishbody):number[]{
        let result:number[]=[];
        result.push(15+Math.round(Math.random() * 950));
        result.push(15+Math.round(Math.random() * 730));
        result.push(Math.random() > 0.8 ? 1 : 0);
        return result;
    }
    /** returns a set of 3 inputs representing location toward the other body */
    static towardLocation(self:Fishbody,other:Fishbody):number[]{

        let result:number[]=[];
        result.push(other.x);
        result.push(other.y);
        result.push(Math.random() > 0.8 ? 1 : 0);
        return result;
    }
    /** returns a set of 3 inputs representing location away from the other body */
    static awayFromLocation(self:Fishbody,other:Fishbody):number[]{

        let deltax = self.x - other.x;
        let deltay = self.y - other.y;
        let deltalen = Math.sqrt(deltax*deltax+deltay*deltay);
        if(deltalen > 3) {
            deltax /= deltalen;
            deltay /= deltalen;
        }
        let result:number[]=[];
        result.push(self.x + deltax*100);
        result.push(self.y + deltay*100);
        result.push(Math.random() > 0.2 ? 1 : 0);
        return result;
    }
    static one():number {
        return 1;
    }
}
