import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { BridgeService } from '../bridge.service'

@Component({
  selector: 'helper',
  templateUrl: './helper.component.html',
  styleUrls: ['./helper.component.css']
})
export class HelperComponent implements OnInit {

  selectable: boolean = true;
  selectfunc:string;
  selectionparam:string;
  funcs = [
    { name: 'max', color: 'primary', selected: true },
    { name: 'filter', color: 'primary', selected: false },
    { name: 'sort', color: 'primary', selected: false },
    { name: 'map', color: 'primary', selected: false },
  ];
  params = [
    { name: 'filterFood', color: 'accent', selected: false },
    { name: 'filterLiving', color: 'accent', selected: false },
    { name: 'filterCanEat', color: 'accent', selected: false },
    { name: 'filterCanBeEaten', color: 'accent', selected: false },
    { name: 'scoringDistance', color: 'accent', selected: false },
    { name: 'sortingDistance', color: 'accent', selected: false },
    { name: 'randomLocation', color: 'accent', selected: false },
    { name: 'towardLocation', color: 'accent', selected: false },
    { name: 'awayFromLocation', color: 'accent', selected: false },
    { name: 'one', color: 'accent', selected: true },
  ];
  constructor(public bridge: BridgeService, private changeDetectionRef : ChangeDetectorRef) 
  { 
    bridge.funcString = "max";
    bridge.paramString = "one";
  }
  
  ngOnInit() {
  }
  ngAfterViewChecked() : void {
    this.changeDetectionRef.detectChanges();
  }
  selectFunc($event) {
  };
  selectParam($event) {
  };

  changeFunc(chip) {
    let vm =this;
    setTimeout(function() {
      chip.selected = !chip.selected
      vm.bridge.funcString = chip.name;
      vm.changeDetectionRef.detectChanges();
    },30);
  }
  changeParam(chip) {
    let vm =this;
    setTimeout(function() {
      chip.selected = !chip.selected
      vm.bridge.paramString = chip.name;
      vm.changeDetectionRef.detectChanges();
    },30);
  }
}
