/** This class controls the game, and feeds ticks to other systems */

import * as PIXI from 'pixi.js';
import * as FISHES from './fishes'
import * as COLLIDERS from './colliders'
import * as MAKERS from './makers'
import { FuzzyLib } from './fuzzylib'
import { BrainFiltering, BrainScoring, FuzzyQuery } from './fuzzyquery'
import { BrainController } from './controllers';
import { Fishbrain } from './fishbrain';
import { Fuzzybrain } from './fuzzybrain';
import { Fishbody } from './fishbody';
import { Movement } from './fishphysics';

export class Boss {
    public colliders: COLLIDERS.Collider[] = [];
    public foodlist: FISHES.Fish[] = [];
    public corpselist: FISHES.Fish[] = [];
    public bodies: Fishbody[] = [];
    public brains: Fishbrain[] = [];
    private maker: MAKERS.Maker;
    private stage: any;
    private renderer: any;
    public mouseX: number;
    public mouseY: number;
    public controllers: BrainController[] = [];
    private canvas: HTMLElement;
    public leftX: number = 50;
    public rightX: number = 980;
    public width: number;
    public topY: number = 50;
    public bottomY: number = 720;
    public height: number;
    private cursor: PIXI.Graphics;
    constructor(stage: any, renderer: any, canvas: HTMLElement) {
        this.stage = stage;
        this.renderer = renderer;
        this.canvas = canvas;
        this.width = this.rightX - this.leftX;
        this.height = this.bottomY - this.topY;
        this.maker = new MAKERS.Maker();
    }

    launch(): void {

        for (var i = 0; i < 9; i++) {
            let bc = new BrainController();
            if (i == 0) {
                bc.brain = new Fuzzybrain();
            } else {
                bc.brain = new Fishbrain();
            }
            this.brains.push(bc.brain);
            this.controllers.push(bc);
        }
        this.tick(0);
    }
    randomX(): number {
        return Math.floor(this.leftX * 100 + Math.random() * this.width * 100);
    }
    randomY(): number {
        return Math.floor(this.topY * 100 + Math.random() * this.height * 100);
    }

    /** Spawn a moving fish that should become associated to a controller */
    addFish(): FISHES.Fish {
        let spawnx = this.randomX();
        let spawny = this.randomY();
        let size = 4 + Math.floor(Math.random() * 10);

        let fish = this.maker.createFish(spawnx, spawny, size);
        fish.setTint("");
        fish.body.food = false;
        this.stage.addChild(fish.spriteBase);
        this.stage.addChild(fish.spriteColor);
        this.colliders.push(fish.collider);
        this.bodies.push(fish.body);
        return fish;
    }
    /** Spawn a food fish that doesnt move, can be from the boss, or from a dead fish */
    addFood(x: number = 0, y: number = 0, min: number = 1, max: number = 4, tint: string = ""): FISHES.Fish {
        let spawnx = x > 0 ? x : this.randomX();
        let spawny = y > 0 ? y : this.randomY();
        let size = Math.floor(min + Math.random() * (max - min));

        let fish = this.maker.createFish(spawnx, spawny, size);
        fish.setTint(tint);
        fish.body.food = true;
        fish.ph.lifetime += Math.random() * 2000;
        fish.ph.setMovement(Movement.Food);
        this.stage.addChild(fish.spriteBase);
        this.stage.addChild(fish.spriteColor);
        this.colliders.push(fish.collider);
        this.bodies.push(fish.body);
        return fish;
    }

    tick(millis: number) {
        this.mouseX = this.renderer.plugins.interaction.mouse.global.x,
            this.mouseY = this.renderer.plugins.interaction.mouse.global.y;

        /** spawn a food that will expire in 20 seconds */
        if (this.foodlist.length < 20) {
            let food = this.addFood();
            food.ph.ttl = 20000;
            this.foodlist.push(food);
        }
        /** tick the food and clean up death */
        for (let it in this.foodlist) {
            let food = this.foodlist[it];
            food.tick(millis);
            if (food.isDead()) {
                this.handleDeath(food);
            }
        }
        /** prune dead entities from list */
        this.foodlist = this.foodlist.filter(function (f) {
            return !f.isDead();
        });

        /** tick the corpse food and clean up death */
        for (let it in this.corpselist) {
            let food = this.corpselist[it];
            food.tick(millis);
            if (food.isDead()) {
                this.handleDeath(food);
            }
        }
        /** prune dead entities from list */
        this.corpselist = this.corpselist.filter(function (f) {
            return !f.isDead();
        });

        /** tick the controllers, which will tick the fish */
        for (let it in this.controllers) {
            let user = this.controllers[it];
            user.tick(millis);
            if (user.fish != null && user.fish.isDead()) {
                this.handleDeath(user.fish);
                user.fish = null;
            }
            /** Make a new fish if the current fish is null */
            if (user.fish == null) {
                let fish = this.addFish();
                user.fish = fish;
                user.brain.selfbody = fish.body;
                user.spawn(fish.ph.x, fish.ph.y);
            }
        }
        this.colliders = this.colliders.filter(function (c) {
            return c.enabled;
        });
        /** lazy circle collision check, it checks i to every j, which is n-squared */
        for (let it in this.colliders) {
            let first = this.colliders[it];
            for (let jt in this.colliders) {
                if (jt != it) {
                    let second = this.colliders[jt];
                    let diffx = second.x - first.x;
                    let diffy = second.y - first.y;
                    let differenceSquared = diffx * diffx + diffy * diffy;
                    let radiuses = (first.r + second.r);
                    if (differenceSquared < radiuses * radiuses) {
                        // throw overlap event
                        this.handleOverlap(first, second);
                    }
                }
            }
        }
        /** prune list of dead bodies */
        this.bodies = this.bodies.filter(function (b) {
            return b.hp > 0;
        });
    }
    handleOverlap(first: COLLIDERS.Collider, second: COLLIDERS.Collider) {
        let sizeDifference = (first.r - second.r) / 200;
        /** If the two entities are sorta similar in size, the smaller one will be eaten while being attached */
        if (sizeDifference >= 1 && sizeDifference <= 5) {
            second.enabled = false;
            second.ph.setMovement(Movement.Attached);
            second.ph.pph = first.ph;
            first.ph.children.push(second.id);
        }
        return;
    }
    handleDeath(fish: FISHES.Fish) {
        if (fish.ph.pph != null) {
            let otherfish = this.findFishID(fish.ph.pph.id);
            if (otherfish) {
                /** remove all instances of self from the parents children */
                otherfish.ph.children = otherfish.ph.children.filter(function (n) {
                    return n != fish.id;
                });
                /** the parent grows in size after finishing the meal */
                otherfish.body.size++;
            }
        } else {
            /** without a parent, died of starvation */
            if (fish.body.size > 8) {
                /** create corpse food for other people to eat */
                for (let i = 0; i < fish.body.size / 4; ++i) {
                    let food = this.addFood(
                        fish.ph.x + Math.random() * 1500,
                        fish.ph.y + Math.random() * 1500,
                        fish.body.size / 3,
                        fish.body.size / 2,
                        fish.tint);
                    /** corpse food expires after 10 seconds */
                    food.ph.ttl = 10000;
                    this.corpselist.push(food);
                }
            }
        }
        /** clean up variables and rendering */
        fish.body.hp = 0;
        fish.collider.enabled = false;
        fish.ph.setMovement(Movement.Dead);
        this.stage.removeChild(fish.spriteBase);
        this.stage.removeChild(fish.spriteColor);
        return;
    }
    /** retrieve the fish from a given id */
    findFishID(id: number): FISHES.Fish {
        for (let it in this.controllers) {
            let user = this.controllers[it];
            if (user.fish != null && user.fish.id == id) {
                return user.fish;
            }
        }
        return null;
    }
    /** retrieve the first controller to be used as the focus controller */
    findFirstBrain(): BrainController {
        if (this.controllers.length > 0) {
            return this.controllers[0];
        }
        return null;

    }



}