/** query structure in the fuzzy brain library */
import { Fishbody } from './fishbody'
import { map } from 'rxjs/operator/map';

export type BrainScoring = (self: Fishbody, _: Fishbody) => number;
export type BrainFiltering = (self: Fishbody, _: Fishbody) => boolean;

export class FuzzyQuery {

    private _parent:FuzzyQuery;
    private _filter:(self:any,_:any) => boolean;
    private _map:(self:any,_:any) => any;
    private _sort:(self:any,lhs:any,rhs:any) => any;
    private _maxcount:number;
    private _name:string;
    constructor(parent:FuzzyQuery = null) {
        this._parent = parent;
        this._filter = (s,n) => {return true};
        this._map = (s,n) => {return n};
        this._sort = null;
        this._maxcount = 9999;
    }
    /** recursive function which grabs all data from ancestor queries */
    get(self:any, others:any[]):any {
        
        let result:any[] = [];
        if(this._parent != null) {
            others = this._parent.get(self,others);
        }
        let size = others.length;
        for(let it in others) {
            if(others[it] == self) {
                continue;
            }
            if(this._filter(self,others[it])) {
                result.push(this._map(self,others[it]));
            }
        }
        if(this._sort != null && result.length > 0) {
            let compareWithSelf = (query, self) => {
                return function(lhs, rhs) {
                    return query._sort(self,lhs,rhs);
                };
            }
            result.sort(compareWithSelf(this,self));
        }
        if(result.length > this._maxcount) {
            return result.slice(0,this._maxcount);
        }
        return result;
    }

    /** create a new query with a maximum count function */
    max(ma:()=>number) {
        let result = new FuzzyQuery(this);
        result._name = "max";
        result._maxcount = ma();
        return result;
    }
    /** create a new query with a filter function */
    filter(fi:(self:any,_:any) => boolean) {
        let result = new FuzzyQuery(this);
        result._name = "filter"+fi.name;
        result._filter = fi;
        return result;
    }
    /** create a new query with a map function */
    map(ma:(self:any,_:any) => any) {
        let result = new FuzzyQuery(this);
        result._name = "map"+ma.name;
        result._map = ma;
        return result;
    }
    /** create a new query with a sort function */
    sort(so:(self:any,lhs:any,rhs:any) => any) {
        let result = new FuzzyQuery(this);
        result._name = "sort"+so.name;
        result._sort = so;
        return result;
    }
}
