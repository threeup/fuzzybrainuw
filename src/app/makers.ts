/** coordinate with the rendering to produce some images and lines */
import * as PIXI from 'pixi.js';
import * as FISHES from './fishes'

export class Maker {
    public nextID:number;
    public basetex:PIXI.Texture;
    public colortex:PIXI.Texture;
    constructor() {
        this.nextID = 0;
        this.basetex = PIXI.Texture.fromImage('assets/fishbase.svg');
        this.basetex.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
        this.colortex = PIXI.Texture.fromImage('assets/fishcolor.svg');
        this.colortex.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
    }
    createFish(x, y, size) : FISHES.Fish {
        let fishbase = new PIXI.Sprite(this.basetex);
        fishbase.anchor.x = 0.5;
        fishbase.anchor.y = 0.51;
        let fishcolor = new PIXI.Sprite(this.colortex);
        fishcolor.anchor.x = 0.5;
        fishcolor.anchor.y = 0.51;
        this.nextID++;
        return new FISHES.Fish(this.nextID, size, fishbase, fishcolor, x, y);
    }

    createLine(sx, sy, fx, fy) : PIXI.Graphics {

        let myline = new PIXI.Graphics();
        
        // Move it to the beginning of the line
        myline.position.set(sx, sy);
        
        // Draw the line (endPoint should be relative to myGraph's position)
        myline.lineStyle(9, 0xffffff)
               .moveTo(0, 0)
               .lineTo(fx-sx, fy-sy);
        myline.endFill();
        return myline;
    }

    moveLine(myline:PIXI.Graphics,sx, sy, fx, fy) : void {

        
        // Move it to the beginning of the line
        myline.position.set(sx, sy);
        
        // Draw the line (endPoint should be relative to myGraph's position)
        myline.lineStyle(9, 0xffffff)
               .moveTo(0, 0)
               .lineTo(fx-sx, fy-sy);
               
        myline.endFill();
    }
}